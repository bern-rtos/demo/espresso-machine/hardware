# Hardware

This repository contains all the schematics and PCB designs developed for the
espresso machine application.

You can find PDF exports of the schematics in the [Release section](https://gitlab.com/bern-rtos/demo/espresso-machine/hardware/-/releases).


## Prerequisites
- Altium Designer

## Authors
- Stefan Lüthi

## License
Copyright Stefan Lüthi 2022.
This documentation describes Open Hardware and is licensed under the
CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms of the
CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for applicable
conditions
